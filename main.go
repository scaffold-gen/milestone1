package main

import (
	"flag"
	"fmt"
	"io"
	"os"
	"strings"
)

type UserFlags struct {
	Name       *string
	Location   *string
	Repository *string
	Static     *bool
}

type ValidationErrors []string

func (v *ValidationErrors) Error() string {
	return strings.Join(*v, "\n")
}

func (v *ValidationErrors) HasErrors() bool {
	return len(*v) > 0
}

func (v *ValidationErrors) Add(err string) {
	*v = append(*v, err)
}

func ParseFlags(arguments []string) (UserFlags, error) {
	flags := UserFlags{}

	fs := flag.NewFlagSet("main", flag.ExitOnError)
	flags.Name = fs.String("n", "", "Project's name")
	flags.Location = fs.String("d", "", "Project's location on disk")
	flags.Repository = fs.String("r", "", "Project's remote repository")
	flags.Static = fs.Bool("s", false, "Enable static assets")
	if err := fs.Parse(arguments); err != nil {
		return UserFlags{}, fmt.Errorf("parsing flags: %w", err)
	}

	var err ValidationErrors
	if flags.Name == nil || *flags.Name == "" {
		err.Add("project name can't be empty")
	}

	if flags.Location == nil || *flags.Location == "" {
		err.Add("project path can't be empty")
	}

	if flags.Repository == nil || *flags.Repository == "" {
		err.Add("project repository can't be empty")
	}

	if err.HasErrors() {
		return UserFlags{}, &err
	}

	return flags, nil
}

func run(w io.Writer, ew io.Writer, arguments []string) error {
	flags, err := ParseFlags(arguments)
	if err != nil {
		fmt.Fprintln(ew, err.Error())
		return err
	}
	fmt.Fprintf(w, "Generating scaffold for project %s in %s\n", *flags.Name, *flags.Location)

	return nil
}

func main() {
	if len(os.Args) == 1 {
		fmt.Printf("Not enough of arguments provided. Please run %s -h for usage information.\n", os.Args[0])
		os.Exit(1)
	}

	if err := run(os.Stdout, os.Stderr, os.Args[1:]); err != nil {
		os.Exit(1)
	}
}
