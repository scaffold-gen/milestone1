package main

import (
	"bytes"
	"strings"
	"testing"
)

func TestSetupParseFlags(t *testing.T) {
	t.Parallel()

	tt := []struct {
		flags     string
		expOut    string
		expErrOut string
	}{
		{
			flags:     "-d ./project1 -n Project1 -r github.com/username/project1",
			expOut:    "Generating scaffold for project Project1 in ./project1\n",
			expErrOut: "",
		},
		{
			flags:     "-n Project1",
			expOut:    "",
			expErrOut: "project path can't be empty\nproject repository can't be empty\n",
		},
	}

	for _, tc := range tt {
		out, errOut := bytes.Buffer{}, bytes.Buffer{}

		t.Run(tc.flags, func(t *testing.T) {
			run(&out, &errOut, strings.Split(tc.flags, " "))
			if got, exp := out.String(), tc.expOut; got != exp {
				t.Errorf("out: expected %v, but got %v", exp, got)
			}
			if got, exp := errOut.String(), tc.expErrOut; got != exp {
				t.Errorf("err: expected %v, but got %v", exp, got)
			}
		})
	}
}
